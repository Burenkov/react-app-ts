export interface IArticle {
  id: number;
  title: string;
  body: string;
  image: string;
  tags: string[];
  created_at: string;
}

export interface IComment {
  id: number;
  name: string;
  avatar: string;
  text: string;
}

class Api {
  url: string;

  constructor(url: string) {
    this.url = url;
  }
  getPosts = async (): Promise<IArticle[]>  => {
    const res = await fetch(`${this.url}/posts`);
    return res.json();
  }
  getPost = async (id: string | number): Promise<IArticle> => {
    const res = await fetch(`${this.url}/posts/${id}`);
    return res.json();
  }
  getPostComments = async (id: string | number): Promise<IComment[]> => {
    const res = await fetch(`${this.url}/posts/${id}/comments`);
    return res.json()
  }
}

const url = "https://61391974163b56001703a24a.mockapi.io/api"
const api = new Api(url);

export {
  api
}
