import { Link } from "react-router-dom"

interface IArticle {
  title: string;
  id: number;
  image: string;
  created_at: string;
}

const ArticleCart = ({
  title,
  id,
  image,
  created_at
}: IArticle) => {
  return (
      <article className="post-card-wrap column medium-6 large-4">
        <div className="post-card  post-card--featured ">
          <Link to={`/post/${id}`} className="block">
            <div className="post-card__image CoverImage FlexEmbed FlexEmbed--4by3"
                 style={{ backgroundImage: `url(${image})` }}>
              <span title="Featured Post">
                <div className="icon icon--ei-star icon--s post-card--featured__icon">
                  <svg className="icon__cnt"><use xlinkHref="#ei-star-icon"></use></svg>
                </div>
              </span>
            </div>
          </Link>
          <div className="post-card__info">
            <div className="post-card__meta">
              <span className="post-card__meta__date">{new Intl.DateTimeFormat().format(new Date(created_at))}</span>
            </div>
            <h2 className="post-card__title">
              <Link to={`/post/${id}`}>{title}</Link>
            </h2>
          </div>
        </div>
      </article>
  )
}

export {
  ArticleCart
}
