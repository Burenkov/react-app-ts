import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import './styles/App.css';
import {Svgs} from "./views/Svgs";
import {AboutPage, HomePage, ArticlePage} from "./pages";

function App() {
  return (
    <Router>
      <Svgs />
      <Switch>
        <Route path="/about">
          <AboutPage />
        </Route>
        <Route path="/post/:id">
          <ArticlePage />
        </Route>
        <Route path="/">
          <HomePage />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
