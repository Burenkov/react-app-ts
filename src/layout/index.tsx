import React from "react";

const Layout: React.FC = ({children}) => {
  return (
    <div className={"off-canvas-container"}>
      <header className="site-header">
        <div className="row">
          <div className="column small-8 medium-3 large-3">
            <h1 className="logo"><a href="/">Midan</a></h1>
          </div>
          <label className="off-canvas-toggle">
            <div className="icon icon--ei-navicon icon--s">
              <svg className="icon__cnt">
                <use xlinkHref="#ei-navicon-icon"></use>
              </svg>
            </div>
          </label>
          <div className="off-canvas-content">
            <div className="column medium-9 large-9">
              <nav className="site-header__navigation navigation">
                <ul className="list-bare">
                  <li>
                    <a className="page-link" href="/about.html">About</a>
                  </li>
                  <li>
                    <a className="page-link" href="/style-guide.html">Style Guide</a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </header>
      <div className="wrapper">
        <div className={"row"}>{children}</div>
      </div>
      <footer className="footer">
        <div className="row">
          <div className="column large-12 footer__section">
            <nav className="navigation">
              <ul className="list-bare footer__nav text-center">
                <li>
                  <a className="page-link" href="/about.html">About</a>
                </li>
                <li>
                  <a className="page-link" href="/style-guide.html">Style Guide</a>
                </li>
                <li><a className="subscribe-button icon-feed"
                       href="http://midan-jekyll.aspirethemes.com/feed.xml">RSS</a></li>
              </ul>
            </nav>
          </div>
          <div className="column large-12 footer__section">
            <ul className="list-bare footer__nav social-icons text-center">
              <li>
                <a href="http://www.twitter.com/aspirethemes" target="_blank">
                  <div className="icon icon--ei-sc-twitter icon--s ">
                    <svg className="icon__cnt">
                      <use xlinkHref="#ei-sc-twitter-icon"></use>
                    </svg>
                  </div>
                </a>
              </li>
              <li>
                <a href="http://www.github.com/aspirethemes" target="_blank">
                  <div className="icon icon--ei-sc-github icon--s ">
                    <svg className="icon__cnt">
                      <use xlinkHref="#ei-sc-github-icon"></use>
                    </svg>
                  </div>
                </a>
              </li>
              <li>
                <a href="mailto:your-email@domain.com" target="_blank">
                  <div className="icon icon--ei-envelope icon--s ">
                    <svg className="icon__cnt">
                      <use xlinkHref="#ei-envelope-icon"></use>
                    </svg>
                  </div>
                </a>
              </li>
            </ul>
          </div>
          <div className="column large-12 footer__section">
            <div className="text-center">
              <div className="font-tiny">
                © 2020 Midan
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}

export {
  Layout
}
