import {useEffect, useState} from "react";
import {api, IComment} from "../../api";

const Comments = ({ id }) => {
  const [comments, setComments] = useState<IComment[]>([]);
  useEffect(() => {
    api.getPostComments(id)
        .then((data) => {
          setComments(data);
        });
  }, [id])

  return (
    <div>
      {comments.map((item) => {
        return (
            <div key={item.id} className="box box--author">
              <figure className="author-image">
                <div className="img"
                     style={{backgroundImage: `url(${item.avatar})`}}>
                  <span className="hidden">{item.name} Picture</span>
                </div>
              </figure>
              <div className="box__body">
                <h4 className="box__title">
                  {item.name}
                </h4>
                <p className="box__text">{item.text}</p>
              </div>
            </div>
        )
      })}
    </div>
  )
}

export {
  Comments
}
