import { useParams } from "react-router-dom";
import {useEffect, useState} from "react";
import {api, IArticle} from "../../api";
import {Share} from "../Share";
import {Comments} from "../Comments";

const Article = () => {
  const [ article, setArticle ] = useState<IArticle | null>(null);
  const { id } = useParams<{ id: string }>();
  useEffect(() => {
    api.getPost(id)
        .then((data) => {
          setArticle(data);
        });
  }, [id])

  if(article === null) {
    return null;
  }
  return (
    <div className="column column--center medium-10 large-10">
      <article className="post">
        <header className="text-center post__header">
          <h2 className="post__title" itemProp="name headline">{article.title}</h2>
          <time className="post__date" dateTime={article.created_at} itemProp="datePublished">
            {new Intl.DateTimeFormat().format(new Date(article.created_at))}
          </time>
        </header>
        <div className="post-content" itemProp="articleBody" dangerouslySetInnerHTML={{__html: article.body}} />
        <div className="row">
          <div className="column large-10">
            <div className="post__tags">
              {article.tags.map((tag) => <a href="/tag/frontpage" key={tag}>{tag}</a>)}
            </div>
          </div>
          <div className="column large-2">
            <Share />
          </div>
        </div>
        <hr/>
        <Comments id={id} />
        <hr/>
      </article>
    </div>
  )
}

export {
  Article
}
