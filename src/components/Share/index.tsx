const Share = () => {
  return (
    <ul className="share-list">
      <li>
        <a className="share-list__link"
           href="https://twitter.com/intent/tweet?text=Welcome to Jekyll!&amp;url=http://midan-jekyll.aspirethemes.com/2016/04/25/welcome-to-jekyll/&amp;via=aspirethemes&amp;related=aspirethemes"
           rel="nofollow" target="_blank" title="Share on Twitter">
          <div className="icon icon--ei-sc-twitter icon--s share-list__icon share-list__icon--twitter">
            <svg className="icon__cnt">
              <use xlinkHref="#ei-sc-twitter-icon"></use>
            </svg>
          </div>
        </a>
      </li>
      <li>
        <a className="share-list__link"
           href="https://facebook.com/sharer.php?u=http://midan-jekyll.aspirethemes.com/2016/04/25/welcome-to-jekyll/"
           rel="nofollow" target="_blank" title="Share on Facebook">
          <div className="icon icon--ei-sc-facebook icon--s share-list__icon share-list__icon--facebook">
            <svg className="icon__cnt">
              <use xlinkHref="#ei-sc-facebook-icon"></use>
            </svg>
          </div>
        </a>
      </li>
    </ul>
  )
}

export {
  Share
}
