import {useEffect, useState} from "react";
import {api, IArticle} from "../../api/";
import {ArticleCart} from "../../views/ArticleCart";

const Articles = () => {
  const [posts, setPosts] = useState<IArticle[]>([]);
  useEffect(() => {
    api.getPosts()
        .then((data) => {
          setPosts(data);
        });
  }, []);

  return (
    <div className={"post-list"}>
      {posts.map(item => (
          <ArticleCart key={item.id} {...item}/>
      ))}
    </div>
  );
}

export {
  Articles
}
