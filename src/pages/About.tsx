import {Layout} from "../layout";

const AboutPage = () => {
  return (
      <Layout>
        About
      </Layout>
  )
}

export default AboutPage;
