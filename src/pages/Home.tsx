import {Layout} from "../layout";
import { Articles } from "../components/Articles";

const HomePage = () => {
  return (
    <Layout>
      <Articles />
    </Layout>
  )
}

export default HomePage;
