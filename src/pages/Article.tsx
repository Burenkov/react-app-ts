import { Article } from "../components/Article";
import { Layout } from "../layout";

const ArticlePage = () => {
  return (
    <Layout>
      <Article />
    </Layout>
  )
}

export default ArticlePage;
