export { default as AboutPage } from "./About";
export { default as HomePage } from "./Home";
export { default as ArticlePage } from "./Article";
